extends Spatial

export var min_show_dist = 2

onready var global = $"/root/GameGlobals"
onready var anim = $AnimationTree["parameters/playback"]

var tracking = false

var target_pos = Vector3.ZERO

func _ready():
	global.connect("mission_start", self, "start")
	global.connect("mission_finish", self, "stop")
	global.connect("mission_fail", self, "stop")

func start():
	anim.travel("appear")
	tracking = true
	target_pos = global.mission.target.global_transform.origin

func stop():
	anim.travel("dissapear")
	tracking = false

func _process(delta):
	var level_target_pos = target_pos
	level_target_pos.y = global_transform.origin.y
	global_transform = global_transform.looking_at(level_target_pos, Vector3.UP)
	if tracking and global.has_mission:
		if global_transform.origin.distance_to(target_pos) < min_show_dist:
			if anim.get_current_node() != "dissapear":
				anim.travel("dissapear")
		else:
			if anim.get_current_node() == "dissapear":
				anim.travel("appear")
