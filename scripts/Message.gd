extends Panel
class_name BaseMessage

var message_content = MessageContent.new()

func get_content(from, to):
	$Title.text = message_content.get_title(from, to)
	$PingText/Content.bbcode_text = message_content.get_body(from, to)

func set_text_color(color: Color):
	$Title.add_color_override("font_color", color)
	$ProgressBar/PingTime.add_color_override("font_color", color)
