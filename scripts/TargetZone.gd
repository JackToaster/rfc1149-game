extends Area
class_name TargetZone

signal finished

export(String) var msg_name
export(String) var msg_location_name
export(String) var msg_ip = null


onready var msg_dest = MessageContent.MsgDestination.new(msg_name, msg_location_name, get_groups(), msg_ip)
onready var global = $"/root/GameGlobals"
onready var zone = $Zone
var time = rand_range(0, 3.14)

var running = false

func start():
	running = true
	zone.visible = true

func stop():
	running = false
	fade_out()

func _ready():
	zone.visible = false

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if running:
		time += delta
		zone.rotate_y(delta)
		zone.transform.origin.y = sin(time * 3.456) * 0.04

func _on_target_zone_body_entered(body):
	if body.is_in_group("Player") and running:
		running = false
		global.finish_mission()
		fade_out()
		emit_signal("finished")

func fade_out():
	var tween = $Tween
	tween.interpolate_property(zone, "transform:origin", zone.transform.origin, Vector3(0, -1, 0), 1)
	tween.start()


func _on_Tween_tween_all_completed():
	zone.visible = false
