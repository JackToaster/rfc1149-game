extends AudioStreamPlayer3D

export(Array, AudioStreamSample) var sounds

#var preloaded_streams = []
#func _ready():
#	for sound in sounds:
#		preloaded_streams.append(preload(sound))

func play_rand(from_position: float = 0.0):
	stream = sounds[floor(rand_range(0, len(sounds)))]
	.play(from_position)
