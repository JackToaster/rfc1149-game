extends Spatial


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func splash(position):
	$Splash.global_transform.origin = position
	$Splash.emitting = true

func spray_pos(position):
	$Spray.global_transform.origin = position

func start_spray():
	$Spray.emitting = true

func stop_spray():
	$Spray.emitting = false
