extends Control

export(float) var autohide_time = 6

var elapsed_time = 0
var paused = false

enum PanelState{
	hidden,
	full,
	partial,
	finished
}
var state = PanelState.hidden

onready var global = $"/root/GameGlobals"
onready var anim_player = $AnimationPlayer

# Called when the node enters the scene tree for the first time.
func _ready():
	global.gui = self
	update_breadcrumbs(global.crumbs, global.breadcrumbs_to_win)
	
	global.connect("mission_start", self, "show_panel")
	global.connect("mission_finish", self, "hide_panel")
	global.connect("mission_fail", self, "fail")
	$AnimationPlayer.play("Hidden")

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if not paused:
		elapsed_time += delta
		_update_timer(elapsed_time)
	if state == PanelState.full:
		if Input.is_action_just_pressed("ui_accept") or Input.is_action_just_pressed("fly"):
			lower_panel()
	elif state == PanelState.partial:
		_update_ping(global.mission.timer * 1000)

func _update_timer(time):
	var minutes = floor(time / 60)
	var seconds = int(floor(time)) % 60
	$TimePanel/TimerLabel.text = "%02d:%02d" % [minutes, seconds]

func _update_timeout(timeout_ms):
	$Message/ProgressBar.max_value = timeout_ms

func _update_ping(ping_ms):
	$Message/ProgressBar/PingTime.text = "%6dms" % int(ping_ms)
	$Message/ProgressBar.value = ping_ms

func update_breadcrumbs(num, out_of):
	$BreadcrumbPanel/BreadcrumbsLabel.text = "%3d/%d" % [num, out_of]

func show_panel():
	paused = true
	get_tree().paused = true
	
#	$Message.free()
#	var new_message = global.mission.message.instance()
#	new_message.set_name("Message")
#	new_message.get_content("foo", "bar")
#	add_child(new_message)
	
	_update_ping(0)
	_update_timeout(global.mission.timeout * 1000)
	
	$AnimationPlayer.play("Raise Panel")
	$AutoHideTimer.start(autohide_time)

func lower_panel():
	$AnimationPlayer.play("Lower Panel")

func hide_panel():
	state = PanelState.finished
	$AnimationPlayer.play("Hide Panel")

func fail():
	state = PanelState.finished
	$AnimationPlayer.play("Fail")

#func message_set_text_color(color: Color):
#	$Message.set_text_color(color)

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "Raise Panel":
		state = PanelState.full
	elif anim_name == "Lower Panel":
		paused = false
		get_tree().paused = false
		state = PanelState.partial


func _on_AutoHideTimer_timeout():
	if state == PanelState.full:
		lower_panel()

func update_message(from, to):
	$Message.get_content(from, to)

func set_message_content(msg_content):
	$Message.message_content = msg_content
	
