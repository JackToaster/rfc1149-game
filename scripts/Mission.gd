extends Object
class_name Mission

var target
var timeout
#var message
var timer = 0
var has_reward

func _init(destination: Area, timeout_seconds: int, reward: bool = true):
	target = destination
	timeout = timeout_seconds
	target.running = true
	has_reward = reward
	#message = msg

func update(delta):
	timer += delta

func timed_out():
	return timer > timeout
