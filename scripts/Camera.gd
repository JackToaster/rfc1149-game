extends Camera

export(float) var velocity_distance_scale_factor = 0.3
export(float) var velocity_scaling_min = 1
export(float) var distance_decay = 0.98
export(float) var angle_decay = 0.97
export(float) var height_increase_factor = 1

export(float) var hide_offset_height = 0.1

export(float) var shake_distance = 0.1

export(bool) var shaking = false

export(NodePath) var character_node
onready var character = get_node(character_node)

var offset
var distance_fac = 1

var hidden_objects = []
var old_materials = {}

onready var y_rotation = character.rotation.y

# Called when the node enters the scene tree for the first time.
func _ready():
	offset = global_transform.origin - character.global_transform.origin
	character.connect("crashed", self, "run_shake_animation")
	shaking = false

func _process(delta):
	distance_fac = distance_decay * distance_fac + (1 - distance_decay) * get_distance_fac(character.linear_velocity)
	var vertical_fac = (distance_fac - 1.0) * height_increase_factor
	#print(vertical_fac)
	if character.state != Player.State.crashing:
		y_rotation = lerp_angle(character.rotation.y, y_rotation, angle_decay)
	var rotated_offset = offset.rotated(Vector3.UP, y_rotation)
	if shaking:
		var random_offset = Vector3(rand_range(-shake_distance, shake_distance), \
			rand_range(-shake_distance, shake_distance), \
			rand_range(-shake_distance, shake_distance))
		global_transform.origin = character.global_transform.origin + rotated_offset * distance_fac + random_offset + Vector3.UP * vertical_fac
	else:
		global_transform.origin = character.global_transform.origin + rotated_offset * distance_fac + Vector3.UP * vertical_fac
	
	global_transform = global_transform.looking_at(character.global_transform.origin, Vector3.UP)

func run_shake_animation():
	$ShakeController.play("Crash")

# Called every physics frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	hide_intersecting_objects()

func hide_intersecting_objects():
	var cam_pos = global_transform.origin
	var player_pos = character.global_transform.origin + Vector3(0, hide_offset_height, 0)
	var space_state = get_world().direct_space_state
	var exceptions = [character]
	var to_hide = []
	while true:
		var collision_result = space_state.intersect_ray(cam_pos, player_pos, exceptions)
		if collision_result.has("collider"):
			exceptions.push_back(collision_result.collider)
			to_hide.push_back(collision_result.collider)
		else:
			break
	
	
	for object in hidden_objects:
		if not object in to_hide:
			unhide_object(object)
			hidden_objects.erase(object)
	
	for object in to_hide:
		if not object in hidden_objects:
			hide_object(object)
			hidden_objects.append(object)

func get_materials(collider: PhysicsBody):
	if collider == null:
		return false
	var parent = collider.get_parent()
	if parent.is_class("VisualInstance"):
		var old_mesh = parent.mesh
		var new_mesh = old_mesh.duplicate()
		var materials = []
		#var old_mats = []
		for i in range(new_mesh.get_surface_count()):
			#var old_mat = mesh.surface_get_material(i)
			var new_mat = new_mesh.surface_get_material(i).duplicate()
			new_mesh.surface_set_material(i, new_mat)
			materials.append(new_mat)
			#old_mats.append(old_mat)
		
		old_materials[collider] = old_mesh
		parent.mesh = new_mesh
		return materials
	return false

func reset_materials(collider: PhysicsBody):
	if collider == null:
		return
	var parent = collider.get_parent()
	if parent.is_class("VisualInstance"):
		#var old_mesh = parent.mesh
		parent.mesh = old_materials[collider]
		#old_mesh.free()
#		var mesh = parent.mesh
#		var materials = old_materials[collider]
#		for i in range(mesh.get_surface_count()):
#			mesh.surface_set_material(i, materials[i])
	old_materials.erase(collider)

func hide_object(object):
	var materials = get_materials(object)
	if not materials:
		return
	for material in materials:
		# Don't cull back faces
		material.params_cull_mode = 2
		material.flags_transparent = true
		material.albedo_color.a = 0.3
	
func unhide_object(object):
	reset_materials(object)

func get_distance_fac(velocity):
	return max(1 + (velocity.length() - 1) * velocity_distance_scale_factor, 1)

