extends Area

export(AudioStreamOGGVorbis) var audio_stream
export(float) var volume_db = -5
export(float) var fade_time = 2.0

func _ready():
	$AudioStreamPlayer.stream = audio_stream

func _on_AmbienceArea_body_entered(body):
	if body.is_in_group("Player"):
		$Tween.interpolate_property($AudioStreamPlayer, "volume_db", -80, volume_db, fade_time, Tween.TRANS_EXPO, Tween.EASE_OUT)
		$Tween.start()
		$AudioStreamPlayer.volume_db = -80
		$AudioStreamPlayer.play()
		#print('playing!')


func _on_AmbienceArea_body_exited(body):
	if body.is_in_group("Player"):
		$Tween.interpolate_property($AudioStreamPlayer, "volume_db", null, -80, fade_time, Tween.TRANS_EXPO, Tween.EASE_IN)
		$Tween.start()
		$AudioStreamPlayer.volume_db = -80
		#print('Stopping!')
