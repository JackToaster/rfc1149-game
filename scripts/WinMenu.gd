extends ColorRect

onready var gui = get_parent()

var tree_paused
var gui_paused

enum State{
	paused,
	unpaused
}
var state = State.unpaused

var accepting_inputs = false

func _ready():
	# This may be the ugliest hack in this entire game.
	var scale_fac = float(OS.window_size.x) / 1024.0
	$ParticleRoot.scale = Vector2(scale_fac, scale_fac)
	_set_time()
	pause()

func _set_time():
	var time = GameGlobals.gui.elapsed_time
	var minutes = floor(time / 60)
	var seconds = time - 60.0 * minutes
	var time_str = "Your time: %02d:%02.2f" % [minutes, seconds]
	$MenuContainer/Panel/Label/Label.text = time_str

func _toggle_pause():
	if state == State.unpaused:
		pause()
	else:
		unpause()

func pause():
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	tree_paused = get_tree().paused
	get_tree().paused = true
	gui_paused = gui.paused
	gui.paused = true
	gui.anim_player.pause_mode = PAUSE_MODE_STOP
	state = State.paused
	$AnimationPlayer.play("Appear")
	$ParticleRoot/Feathers.emitting = true

func unpause():
	$AnimationPlayer.play("Dissapear")

func _unpause():
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	get_tree().paused = tree_paused
	gui.paused = gui_paused
	gui.anim_player.pause_mode = PAUSE_MODE_PROCESS
	state = State.unpaused
	queue_free()

func _on_AnimationPlayer_animation_finished(anim_name):
	match anim_name:
		"Appear":
			pass
		"Dissapear":
			_unpause()


func _on_Resume_pressed():
	if state == State.paused:
		unpause()


enum FadeCompleteAction{
	restart,
	exit
}
var fade_complete_action

func _on_Restart_pressed():
	fade_complete_action = FadeCompleteAction.restart
	accepting_inputs = false
	$FadeRect.play()


func _on_Exit_pressed():
	fade_complete_action = FadeCompleteAction.exit
	accepting_inputs = false
	$FadeRect.play()

func _on_FadeRect_fade_completed():
	match fade_complete_action:
		FadeCompleteAction.restart:
			$"/root/GameGlobals".start_game()
		FadeCompleteAction.exit:
			$"/root/GameGlobals".start_menu()
