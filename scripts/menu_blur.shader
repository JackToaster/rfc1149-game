shader_type canvas_item;

uniform vec4 color: hint_color;
uniform float blur_amnt = 1;

void fragment(){
	COLOR = mix(textureLod(SCREEN_TEXTURE, SCREEN_UV, blur_amnt), color, color.a);
}