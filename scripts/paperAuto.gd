extends Spatial
class_name PaperZoneAuto

export(String) var msg_name
export(String) var msg_location_name
export(Array, NodePath) var exclude_destinations


onready var msg_source = MessageContent.MsgSource.new(msg_name, msg_location_name, get_groups(), self)
onready var zone = $Zone
onready var global = $"/root/GameGlobals"

var picked_up = false
var time = rand_range(0, 3.14)

var hidden = false

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if not picked_up:
		time += delta
		zone.rotate_y(delta)
		zone.transform.origin.y = sin(time * 3.456) * 0.04

func _on_Area_body_entered(body):
	if body.is_in_group("Player") and not picked_up and not global.has_mission:
		picked_up = true
		var target_node = MessageContentFactory.random_target_node(msg_source, get_tree(), exclude_destinations)
		if target_node != null:
			global.gui.set_message_content(MessageContentFactory.auto(msg_source, target_node.msg_dest))
			global.gui.update_message(msg_source, target_node.msg_dest)
			var distance = (msg_source.node.global_transform.origin - target_node.global_transform.origin).length()
			var timeout = global.calc_mission_time(distance)
			global.start_mission(Mission.new(target_node, timeout))
		fade_out()

func fade_in():
	visible = true
	$Paper.visible = true
	var tween = $Tween
	tween.interpolate_property(zone, "transform:origin", Vector3(0, -1, 0), zone.transform.origin, 1)
	tween.start()
	hidden = false
	picked_up = false

func fade_out():
	$Paper.visible = false
	var tween = $Tween
	tween.interpolate_property(zone, "transform:origin", zone.transform.origin, Vector3(0, -1, 0), 1)
	tween.start()
	hidden = true

func _on_Tween_tween_all_completed():
	if hidden:
		visible = false
