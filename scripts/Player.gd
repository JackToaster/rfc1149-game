extends RigidBody
class_name Player

export(float) var walk_forward_speed = 1
export(float) var walk_control_force = 5

export(float) var walk_turn_speed = 1
export(float) var walk_turn_control_torque = 1
export(float) var turn_anim_scale = 0.3

export(Vector3) var jump_impulse = Vector3(0, 3, 3)

export(float) var fly_slow_speed = 3
export(float) var fly_vertical_speed = 1
export(float) var fly_fast_speed = 6
export(float) var fly_control_force = 3
export(float) var fly_turn_speed = 1
export(float) var fly_turn_control_torque = 1
export(float) var landing_height = 0.2
export(float) var autofly_height = 0.6

export(float) var bank_multiplier = 0.4

export(float) var height_limit = 3
export(float) var height_gain = 1
export(float) var height_vel_gain = 10

export(float) var crash_height_min = 0.5
export(float) var crash_impulse_strength = 1
export(float) var crash_recover_time = 4

export(float) var takeoff_delay = 0.5

var fail_on_crash = true
var can_fly = true

onready var character = $character
onready var normal_fricion = friction

enum State{
	walking,
	taking_off,
	flying,
	crashing
}

var state = State.walking
var input_vec

signal crashed

# Called when the node enters the scene tree for the first time.
func _ready():
	set_physics_process(true)
	_set_state(State.walking)
	$"/root/GameGlobals".player = self

func _process(_delta):
	if linear_velocity.length() > 4.5:
		$Trail.fade_in()
	else:
		$Trail.fade_out()

func _physics_process(delta: float):
	#print(state)
	input_vec = get_ui_input_vector()
	
	if state == State.walking:
		walk(input_vec)
	elif state == State.flying:
		fly(input_vec)
	elif state == State.crashing:
		crash()

func _set_state(new_state):
	state = new_state
	if state == State.walking:
		character.walk()
	elif state == State.taking_off:
		character.fly()
		angular_velocity.y = 0
		$TakeoffTimer.start(takeoff_delay)
	elif state == State.flying:
		pass
	elif state == State.crashing:
		emit_signal("crashed")
		
		character.crash()
		axis_lock_angular_x = false
		axis_lock_angular_x = false
		friction = 1
		var torque_dir = Vector3(rand_range(-1, 1), rand_range(-1, 1), rand_range(-1, 1)).normalized()
		apply_torque_impulse(torque_dir * crash_impulse_strength)
		_start_particle_spray()
		$RecoveryTimer.start(crash_recover_time)
		if $"/root/GameGlobals".has_mission and fail_on_crash:
			$"/root/GameGlobals".fail_mission()

func walk(input_vec: Vector3):
	var dir_vec = global_transform.basis.z
	var forward_speed = linear_velocity.dot(dir_vec)
	#print(forward_speed)
	character.set_walk_speed(forward_speed + turn_anim_scale * abs(angular_velocity.y))
	
	# LINEAR
	# Proportional controller for control force
	var forward_input = clamp(input_vec.z, 0, 1)
	var wanted_velocity = forward_input * walk_forward_speed * dir_vec
	var vel_error = wanted_velocity - linear_velocity
	var correction_force = vel_error * walk_control_force
	
	# No vertical force in walk mode
	correction_force.y = 0
	
	add_central_force(correction_force)
	
	# ANGULAR
	var target_angular_vel = input_vec.x * walk_turn_speed
	var angular_vel_error = target_angular_vel - angular_velocity.y
	add_torque(Vector3(0, angular_vel_error * walk_turn_control_torque, 0))
	
	#print('Walking')
	#print('wanted vel: ' + str(wanted_velocity))
	#print('vel error : ' + str(vel_error))
	#print('correction: ' + str(correction_force))
	
	if input_vec.y > 0 or get_ground_dist() > autofly_height:
		if can_fly:
			_set_state(State.taking_off)

func fly(input_vec: Vector3):
	var dir_vec = global_transform.basis.z
	
	var fly_speed = lerp(fly_slow_speed, fly_fast_speed, clamp(input_vec.z, 0, 1))
	
	# Proportional controller for control force
	var wanted_velocity =  fly_speed * dir_vec
	var vel_error = wanted_velocity - linear_velocity
	var correction_force = vel_error * fly_control_force
	
	# Figure out how fast to ascend/descend
	var wanted_vertical_vel = 0
	var height = get_height()
	if input_vec.y > 0:
		var height_error = height_limit - height
		wanted_vertical_vel = clamp(height_error * height_gain, -fly_vertical_speed, fly_vertical_speed)
	else:
		wanted_vertical_vel = -fly_vertical_speed
	
	var vertical_vel_error = wanted_vertical_vel - linear_velocity.y
	
	correction_force.y = weight + vertical_vel_error * height_vel_gain
	
	add_central_force(correction_force)
	
	# ANGULAR
	var target_angular_vel = input_vec.x * fly_turn_speed
	var angular_vel_error = target_angular_vel - angular_velocity.y
	add_torque(Vector3(0, angular_vel_error * fly_turn_control_torque, 0))
	
	$character.rotation.z = angular_velocity.y * bank_multiplier
	
	character.set_flap(input_vec.y > 0 or input_vec.z > 0)
	
	if get_ground_dist() < landing_height and input_vec.y <= 0 and linear_velocity.y <= 0:
		$character.rotation.z = 0
		_set_state(State.walking)
	
	check_crash()

func crash():
	_update_particle_spray()

func _start_particle_spray():
	$"/root/ParticleSystem".splash(global_transform.origin)
	$"/root/ParticleSystem".start_spray()
	$"/root/ParticleSystem".spray_pos(global_transform.origin)

func _stop_particle_spray():
	$"/root/ParticleSystem".stop_spray()
	$"/root/ParticleSystem".splash(global_transform.origin)

func _update_particle_spray():
	$"/root/ParticleSystem".spray_pos(global_transform.origin)

func get_height() -> float:
	var collision_point = $Altimeter.get_collision_point()
	return global_transform.origin.distance_to(collision_point)

func get_ground_dist() -> float:
	var collision_point = $landingAltimeter.get_collision_point()
	return global_transform.origin.distance_to(collision_point)


func get_ui_input_vector() -> Vector3:
	var input_vector = Vector3()
	
	if Input.is_action_pressed("ui_up"):
		input_vector += Vector3(0, 0, 1)
	if Input.is_action_pressed("ui_down"):
		input_vector += Vector3(0, 0, -1)
	if Input.is_action_pressed("ui_left"):
		input_vector += Vector3(-1, 0, 0)
	if Input.is_action_pressed("ui_right"):
		input_vector += Vector3(1, 0, 0)
	
	if Input.is_action_pressed("fly"):
		input_vector += Vector3(0, 1, 0)
	
	return input_vector


func _on_Takeoff_Timer_timeout():
	apply_central_impulse(global_transform.basis.xform(jump_impulse))
	call_deferred("_set_state", State.flying)

func _on_RecoveryTimer_timeout():
	_stop_particle_spray()
	friction = normal_fricion
	#rotation = Vector3(0, rotation.y, 0)
	axis_lock_angular_x = true
	axis_lock_angular_z = true
	$RecoveryTween.interpolate_property(self, "rotation", rotation, Vector3(0, rotation.y, 0), 0.5, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	$RecoveryTween.start()
	
func _on_RecoveryTween_tween_all_completed():
	_set_state(State.walking)



## New crash detection - keeps track of the number of colliding bodies
# so you can't fly with your head in a wall.
var colliding_bodies = 0

func _on_FlightCollisionArea_body_entered(body):
	colliding_bodies += 1

func _on_FlightCollisionArea_body_exited(body):
	colliding_bodies -= 1

func check_crash():
	#print(colliding_bodies)
	if colliding_bodies > 1 and get_height() > crash_height_min:
		$character.rotation.z = 0
		_set_state(State.crashing)
