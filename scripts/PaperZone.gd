extends Spatial
class_name PaperZone

enum MsgType{
	automatic,
	tutorial,
	binary,
	email
}

export(NodePath) var target_area
export(float) var mission_timeout = 30
export(String) var msg_name
export(String) var msg_location_name
export(MsgType) var message_type = MsgType.automatic
export(bool) var reward


onready var msg_source = MessageContent.MsgSource.new(msg_name, msg_location_name, get_groups(), self)
onready var zone = $Zone
onready var global = $"/root/GameGlobals"

var picked_up = false
var time = rand_range(0, 3.14)

var hidden = false

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if not picked_up:
		time += delta
		zone.rotate_y(delta)
		zone.transform.origin.y = sin(time * 3.456) * 0.04

#func _get_message():
#	return MessageContent.new()

func _on_Area_body_entered(body):
	if body.is_in_group("Player") and not picked_up and not global.has_mission:
		picked_up = true
		var target_node = get_node_or_null(target_area)
		if target_node != null:
			global.gui.set_message_content(MessageContentFactory.new_type(message_type))
			global.gui.update_message(msg_source, target_node.msg_dest)
			global.start_mission(Mission.new(target_node, mission_timeout, reward))
		fade_out()

func fade_in():
	visible = true
	$Paper.visible = true
	var tween = $Tween
	tween.interpolate_property(zone, "transform:origin", Vector3(0, -1, 0), zone.transform.origin, 1)
	tween.start()
	hidden = false

func fade_out():
	$Paper.visible = false
	var tween = $Tween
	tween.interpolate_property(zone, "transform:origin", zone.transform.origin, Vector3(0, -1, 0), 1)
	tween.start()
	hidden = true

func _on_Tween_tween_all_completed():
	if hidden:
		visible = false
