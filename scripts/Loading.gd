extends ColorRect

func set_percent(pct: float):
	var pct_int = int(floor(pct))
	$percent.text = str(pct_int) + "%"
