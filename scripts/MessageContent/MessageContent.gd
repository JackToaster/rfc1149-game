extends Object
class_name MessageContent

class MsgSource:
	var name: String
	var location_name: String
	var groups
	var node
	func _init(name_, location_name_, groups_, node_):
		name = name_
		location_name = location_name_
		groups = groups_
		node = node_

class MsgDestination:
	extends MsgSource
	var ip: String
	func _init(name_, location_name_, groups_, node_, ip_=null).(name_, location_name_, groups_, node_):
		if ip_ == null:
			ip = _gen_ip()
		else:
			ip = ip_
	
	func _gen_ip() -> String:
		return "%d.%d.%d.%d" % [int(rand_range(1, 255)), int(rand_range(1, 255)), int(rand_range(1, 255)), int(rand_range(1, 255))]


const body_fmt_string = "SEND TO: %s\n(%s)\nContent:\n%s"

# warning-ignore:unused_argument
# warning-ignore:unused_argument
func get_title(from: MsgSource, to: MsgDestination):
	return 'Message from %s to %s' % [from.name, to.name]

# warning-ignore:unused_argument
func get_body(from: MsgSource, to: MsgDestination):
	return _format_body(to.location_name, to.ip, 'placeholder content')

func _format_body(send_to, ip, content):
	return body_fmt_string % [send_to, ip, content]
