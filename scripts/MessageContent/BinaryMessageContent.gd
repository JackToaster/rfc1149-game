extends MessageContent
class_name BinaryMessageContent

const hex = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9',\
	'A', 'B', 'C', 'D', 'E', 'F', 'BEEF', 'C0FEE', 'D1E5E1', 'B00B5', '1D1075', \
	'1CE', '1A1A1A', 'DE1E7E', 'BADA55', 'A55', '666', '1234', 'F00', 'F000',\
	'F0000', 'FACADE', 'FACE', 'BEAD', 'DEAD', 'D1E', '5318008']

# warning-ignore:unused_argument
# warning-ignore:unused_argument
func get_title(from: MsgSource, to: MsgDestination):
	return 'Message to %s' % to.location_name

# warning-ignore:unused_argument
func get_body(from: MsgSource, to: MsgDestination):
	var content = ''
	for i in range(256):
		content = content + _random_hex()
	
	return ._format_body(to.location_name, to.ip, content)

func _random_hex():
	var rand = floor(rand_range(0, len(hex)))
	return hex[rand]
