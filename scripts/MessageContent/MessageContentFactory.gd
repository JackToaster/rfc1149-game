extends Node

enum MsgType{
	automatic,
	tutorial,
	binary,
	email
}

func new_type(type):
	match type:
		MsgType.automatic:
			return MessageContent.new()
		MsgType.tutorial:
			return TutorialMessageContent.new()
		MsgType.binary:
			return BinaryMessageContent.new()
		MsgType.email:
			return BinaryMessageContent.new()
		_:
			return MessageContent.new()

const group_graph = {
	"SocialMedia": ["Person"],
	"Person": ["SearchEngine", "SocialMedia", "Bank", "Person", "Business"],
	"Business": ["Person"],
	"SearchEngine": ["Person"],
	"Bank": ["Person"],
}

func random_target_node(src, tree, exclude) -> Node:
	var dest_group_choices = []
	for group in src.groups:
		if group in group_graph.keys():
			dest_group_choices = dest_group_choices + group_graph[group]
	
	var dest_choices = []
	for dest_group_choice in dest_group_choices:
		var group_nodes = tree.get_nodes_in_group(dest_group_choice)
		for node in group_nodes:
			if not node in dest_choices and node.is_in_group("Target") and not node in exclude:
				dest_choices.append(node)
	
	# Make destinations deterministic
	rand_seed(hash(src.node.get_instance_id()))
	
	var choice = dest_choices[floor(rand_range(0, len(dest_choices)))]
	
	return choice

onready var type_map = {
	"SocialMedia": {"Person": SocialToPersonMessage.new()},
	"Person": {
		"SearchEngine": PersonToSearchEngine.new(),
		"SocialMedia": PersonToSocialMessage.new(),
		#"Bank": MessageContent.new(),
		"Person": PersonToPersonMessage.new(),
		"Business": PersonToBusinessMessage.new()
	},
	"Business": {"Person": BusinessToPersonMessage.new()},
	"SearchEngine": {"Person": SearchEngineToPerson.new()},
	#"Bank": {"Person": MessageContent.new()},
}

func auto(src, dst):
	if len(src.groups) > 0 and len(dst.groups) > 0:
		var options = []
		for src_group in src.groups:
			if src_group in type_map.keys():
				var src_type_map = type_map[src_group]
				for dst_group in dst.groups:
					if dst_group in src_type_map.keys():
						options.append(src_type_map[dst_group])
		
		if len(options) > 0:
			return options[floor(rand_range(0, len(options)))]
	
	# Should be unreachable, but just in case...........
	return BinaryMessageContent.new()
