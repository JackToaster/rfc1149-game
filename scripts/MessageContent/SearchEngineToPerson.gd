extends MessageContent
class_name SearchEngineToPerson

const contents = [
	'search="how to squirrel proof bird feeder" response="squirrelproofbirdfeeder.com bestsquirreldeterrent.net birdfeedersrus.co.uk"',
	'search="bird egg identification" response="eggs.com cooking.com/omlettes eggcolorgame.net"',
	'search="what time of year do birds fly south" response="wikipedia.org/migration calendar.goggle.com"',
	'search="can birds transmit covid-19" response="cdc.gov/covid19/pigeons widipedia.org/swine-flu',
	'search="today\'s weather" response="weather-forecast.co.nz forecast.net"',
	'search="what is the airspeed velocity of an unladen swallow" resonse="well that really depends if you mean an African swallow or a European swallow"',
]

# warning-ignore:unused_argument
# warning-ignore:unused_argument
func get_title(from: MsgSource, to: MsgDestination):
	return _format_string("Search request from {srcname}", from, to)

# warning-ignore:unused_argument
func get_body(from: MsgSource, to: MsgDestination):
	var content = _format_string(_random_body(), from, to)
	return ._format_body(to.location_name, to.ip, content)

func _format_string(string: String, from: MsgSource, to: MsgDestination) -> String:
	var params = {'srcname': from.name, 'dstname': to.name, 'srclocation': from.location_name, 'dstlocation': to.location_name}
	return string.format(params)

func _random_body() -> String:
	return contents[floor(rand_range(0, len(contents)))]
