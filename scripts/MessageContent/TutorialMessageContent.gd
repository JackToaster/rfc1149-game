extends MessageContent
class_name TutorialMessageContent

# warning-ignore:unused_argument
# warning-ignore:unused_argument
func get_title(from: MsgSource, to: MsgDestination):
	return from.location_name

# warning-ignore:unused_argument
func get_body(from: MsgSource, to: MsgDestination):
	return from.name
