extends MessageContent
class_name PersonToSocialMessage

const contents = {
	'meme0': '[img=530]images/memes/meme0.png[/img]',
	'meme1': '[img=530]images/memes/meme1.png[/img]',
	'meme2': '[img=530]images/memes/meme2.png[/img]',
	'meme3': '[img=530]images/memes/meme3.png[/img]',
	'meme4': '[img=530]images/memes/meme4.png[/img]',
	'meme5': '[img=530]images/memes/meme5.png[/img]',
	'meme6': '[img=530]images/memes/meme6.png[/img]',
	'meme7': '[img=530]images/memes/meme7.png[/img]',
	'meme8': '[img=530]images/memes/meme11.png[/img]',
	'meme9': '[img=530]images/memes/meme9.png[/img]',
	'meme10': '[img=530]images/memes/meme10.png[/img]',
	'meme11': '[img=530]images/memes/meme8.png[/img]',
	'meme12': '[img=530]images/memes/meme12.png[/img]',
	'meme13': '[img=530]images/memes/meme13.png[/img]',
	'video0': 'pigeon-dances-to-elvis.mp4',
	'video1': 'funny-dancing-pigeon.mp4',
	'video2': 'bird-video-for-cats.mp4',
	'video3': 'bird-calls.mp4',
}

const titles = {
	'New post by {srcname}': ['meme0', 'meme1', 'meme2', 'meme3', 'meme4', 'meme5', 'meme6', 'meme7', 'meme8', 'meme9', 'meme10', 'meme11', 'meme12', 'meme13',],
	'Post from {srcname}': ['meme0', 'meme1', 'meme2', 'meme3', 'meme4', 'meme5', 'meme6', 'meme7', 'meme8', 'meme9', 'meme10', 'meme11', 'meme12', 'meme13', ],
	'Post shared by {srcname}': ['meme0', 'meme1', 'meme2', 'meme3', 'meme4', 'meme5', 'meme6', 'meme7', 'meme8', 'meme9', 'meme10', 'meme11', 'meme12', 'meme13', ],
	'Video upload from {srcname}': ['video0', 'video1', 'video3', 'video2', ],
}

var selected_title

# warning-ignore:unused_argument
# warning-ignore:unused_argument
func get_title(from: MsgSource, to: MsgDestination):
	selected_title = _random_title()
	return _format_string(selected_title, from, to)

# warning-ignore:unused_argument
func get_body(from: MsgSource, to: MsgDestination):
#	print('---')
#	print(from.name)
#	print(from.location_name)
#	print('-')
#	print(to.name)
#	print(to.location_name)
	var content = _format_string(_random_body(selected_title), from, to)
	return ._format_body(to.location_name, to.ip, content)

func _format_string(string: String, from: MsgSource, to: MsgDestination) -> String:
	var params = {'srcname': from.name, 'dstname': to.name, 'srclocation': from.location_name, 'dstlocation': to.location_name}
	return string.format(params)

func _random_title() -> String:
	var title_set = titles.keys()
	return title_set[floor(rand_range(0, len(title_set)))]

func _random_body(title) -> String:
	var choices = titles[title]
	return contents[choices[floor(rand_range(0, len(choices)))]]
