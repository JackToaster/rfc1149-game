extends MessageContent
class_name BusinessToPersonMessage

const contents = {
	'spam': 'Hello. I am the son of the crown prince of Nigeria, and I recently came into the posession of a large fortune. I need your help to access the trust fund. Please send $3000 to my bank account to show that you are trustworthy, then I will transfer the remaining money to you as payment for your service.',
	'spam2': 'This is MacroFirm tech support. Our servers have detected 13 viruses on your pc. Please send the four digit code on the back of your credit card to have the viruses removed. If you do not do this in the next 10 minutes your computer will collapse into a microscopic black hole.',
	'spam3': 'Hello mr/mrs{dstname}. I have an interesting business proposition for you. Please reply immediately.',
	'spam4': 'TO THE RESIDENT OF {dstlocation}: You have overdrawn your checking account by $13,377. Please make a wire transfer of $1000 immediately to us to clear your balance. If you fail to make the payment you will be arrested.',
	'spam5': 'Get the only FBA-approved beak enlargement pill on the market! GARANTEED 300% growth in the first week or your money back!',
	'confirmation': 'This email is confirming the order of one (1) bag of birdseed by {dstname}. Your order will be shipped to {dstlocation}.',
	'advert': 'Have you heard the amazing new offer on pet supplies? Come shop for up to 10% off!',
	'password': 'We received a request to reset the password for {dstname}\'s account. Your new password is: Hunter2'
}

const titles = {
	'Email: [URGENT] New mail for {dstname}': ['spam', 'spam2', 'spam3', 'spam4', 'spam5'],
	'Email: YOU WONT BELIEVE THIS NEW OFFER': ['spam', 'spam2', 'spam3', 'spam4', 'spam5', 'advert'],
	'Email: Order confirmation': ['confirmation'],
	'Email: New offer!': ['advert', 'spam5'],
	'Email: Password reset': ['password']
}

var selected_title

# warning-ignore:unused_argument
# warning-ignore:unused_argument
func get_title(from: MsgSource, to: MsgDestination):
	selected_title = _random_title()
	return _format_string(selected_title, from, to)

# warning-ignore:unused_argument
func get_body(from: MsgSource, to: MsgDestination):
#	print('---')
#	print(from.name)
#	print(from.location_name)
#	print('-')
#	print(to.name)
#	print(to.location_name)
	var content = _format_string(_random_body(selected_title), from, to)
	return ._format_body(to.location_name, to.ip, content)

func _format_string(string: String, from: MsgSource, to: MsgDestination) -> String:
	var params = {'srcname': from.name, 'dstname': to.name, 'srclocation': from.location_name, 'dstlocation': to.location_name}
	return string.format(params)

func _random_title() -> String:
	var title_set = titles.keys()
	return title_set[floor(rand_range(0, len(title_set)))]

func _random_body(title) -> String:
	var choices = titles[title]
	return contents[choices[floor(rand_range(0, len(choices)))]]
