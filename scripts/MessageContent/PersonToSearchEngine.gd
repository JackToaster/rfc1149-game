extends MessageContent
class_name PersonToSearchEngine

const contents = [
	'how to squirrel proof bird feeder',
	'bird egg identification',
	'what time of year do birds fly south',
	'can birds transmit covid-19',
	'today\'s weather',
	'where to buy bird seed',
	'how to clean bird poop off of my modem',
	'why isnt the bird seed that I planted growing any birds',
	'ideal water temperature for bird bath',
	'fastest bird mph',
	'how to get pigeon out of house',
	'what is the airspeed velocity of an unladen swallow',
	'{dstname} website'
]

# warning-ignore:unused_argument
# warning-ignore:unused_argument
func get_title(from: MsgSource, to: MsgDestination):
	return _format_string("Search request from {srcname}", from, to)

# warning-ignore:unused_argument
func get_body(from: MsgSource, to: MsgDestination):
	var content = _format_string(_random_body(), from, to)
	return ._format_body(to.location_name, to.ip, content)

func _format_string(string: String, from: MsgSource, to: MsgDestination) -> String:
	var params = {'srcname': from.name, 'dstname': to.name, 'srclocation': from.location_name, 'dstlocation': to.location_name}
	return string.format(params)

func _random_body() -> String:
	return contents[floor(rand_range(0, len(contents)))]
