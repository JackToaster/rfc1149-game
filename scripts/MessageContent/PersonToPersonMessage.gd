extends MessageContent
class_name PersonToPersonMessage

const contents = {
	'txt1': 'lol',
	'txt2': 'Hey {dstname} can I meet u at 6:30?',
	'txt3': 'Did you fill the bird feeder today?',
	'letter': 'Hi {dstname}! Hope you\'re doing well. Sincerely, {srcname}',
	'letter2': 'It\'s me, {srcname}! I haven\'t seen you in so long, {dstname}! Hope you are doing okay.',
	'chain': '&ltIMG src="funny.gif"&gt OMG HAVE YOU SEEN THIS!?!?1 Send it to 5 friends or your dog will explode.',
	'chain2': 'Wow, I tried it and it really works! Forward this email to 10 people and you will recieve a brand new car!',
}

const titles = {
	'Text from {srcname}': ['txt1', 'txt2', 'txt3'],
	'Text to {dstname}': ['txt1', 'txt2', 'txt3'],
	'Message from {srcname}': ['letter', 'letter2'],
	'Email: RE:from {dstname}': ['letter', 'letter2'],
	'Message to {dstname}': ['letter', 'letter2'],
	'Email: RE:to {srcname}': ['letter', 'letter2', 'chain', 'chain2'],
	'Email: FWD:from {srcname}': ['letter', 'letter2', 'chain', 'chain2'],
	'Email: FWD:RE: {dstname}': ['chain', 'chain2']
}

var selected_title

# warning-ignore:unused_argument
# warning-ignore:unused_argument
func get_title(from: MsgSource, to: MsgDestination):
	selected_title = _random_title()
	return _format_string(selected_title, from, to)

# warning-ignore:unused_argument
func get_body(from: MsgSource, to: MsgDestination):
	var content = _format_string(_random_body(selected_title), from, to)
	return ._format_body(to.location_name, to.ip, content)

func _format_string(string: String, from: MsgSource, to: MsgDestination) -> String:
	var params = {'srcname': from.name, 'dstname': to.name, 'srclocation': from.location_name, 'dstlocation': to.location_name}
	return string.format(params)

func _random_title() -> String:
	var title_set = titles.keys()
	return title_set[floor(rand_range(0, len(title_set)))]

func _random_body(title) -> String:
	var choices = titles[title]
	return contents[choices[floor(rand_range(0, len(choices)))]]
