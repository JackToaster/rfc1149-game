extends MessageContent
class_name PersonToBusinessMessage

const contents = {
	'order': '1x"Jumbo XL Bird feeder", 3x"50lb bird seed bag", total=$33',
	'order2': '2x"Squirrel repellant spray for bird feeders", 1x"Medium bird cage", total=$53',
	'return': 'return_item:"Squirrel repellant spray for bird feeders" reason:defective_item comment:"I swear this stuff actually attracts squirrels!"',
	'review': 'rating: ***** \n"Best bird feeder I ever bought. There were so many birds in my yard that it blocked out the sun!"',
	'review2': 'rating:* \n"This squirrel repelant is useless. Not only did the squirrels still overrun my bird feeder, but it made my car\'s engine overheat too!"'
}

const titles = {
	'Shopping order for {dstname}': ['order', 'order2'],
	'Product return request from {srcname}': ['return'],
	'Product review': ['review', 'review2']
}

var selected_title

# warning-ignore:unused_argument
# warning-ignore:unused_argument
func get_title(from: MsgSource, to: MsgDestination):
	selected_title = _random_title()
	return _format_string(selected_title, from, to)

# warning-ignore:unused_argument
func get_body(from: MsgSource, to: MsgDestination):
#	print('---')
#	print(from.name)
#	print(from.location_name)
#	print('-')
#	print(to.name)
#	print(to.location_name)
	var content = _format_string(_random_body(selected_title), from, to)
	return ._format_body(to.location_name, to.ip, content)

func _format_string(string: String, from: MsgSource, to: MsgDestination) -> String:
	var params = {'srcname': from.name, 'dstname': to.name, 'srclocation': from.location_name, 'dstlocation': to.location_name}
	return string.format(params)

func _random_title() -> String:
	var title_set = titles.keys()
	return title_set[floor(rand_range(0, len(title_set)))]

func _random_body(title) -> String:
	var choices = titles[title]
	return contents[choices[floor(rand_range(0, len(choices)))]]
