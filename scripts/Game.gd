extends Node

export(int) var breadcrumbs_to_win = 100

export(int) var crash_penalty = 0
export(float) var reward_per_second = 0.25


export(float) var timeout_seconds_per_meter = 0.3
export(float) var timeout_constant = 10

export(float) var end_timeout_seconds_per_meter = 0.15
export(float) var end_timeout_constant = 3

export(Curve) var difficulty_curve

var player
var gui

var has_mission = false
var mission: Mission

var crumbs = 1337

var has_completed_tutorial = false

var loading_scene = false
var loader = null

onready var loading_screen = preload("res://scenes/Loading.tscn")

func _ready():
	pass

func calc_mission_time(distance):
	var difficulty = difficulty_curve.interpolate(float(crumbs) / float(breadcrumbs_to_win))
	var seconds_per_meter = lerp(timeout_seconds_per_meter, end_timeout_seconds_per_meter, difficulty)
	var constant = lerp(timeout_constant, end_timeout_constant, difficulty)
	return seconds_per_meter * distance + constant

func _reset_all():
	has_mission = false
	mission = null
	crumbs = 0

func add_crumbs(num: int):
	crumbs += num
	if crumbs >= breadcrumbs_to_win and crumbs - num < breadcrumbs_to_win:
		win()
	
	gui.update_breadcrumbs(crumbs, breadcrumbs_to_win)

func remove_crumbs(num: int):
	crumbs -= num
	if crumbs < 0:
		crumbs = 0
		gui.update_breadcrumbs(0, breadcrumbs_to_win)
		lose()
	else:
		gui.update_breadcrumbs(crumbs, breadcrumbs_to_win)

signal mission_start
func start_mission(it: Mission):
	mission = it
	mission.target.start()
	has_mission = true
	emit_signal("mission_start")
	player.character.set_paper_visible(true)
	$PickUpSound.play()
	#gui.show_panel()

signal mission_finish
func finish_mission():
	has_mission = false
	if mission.has_reward:
		add_crumbs(floor(reward_per_second * mission.timeout))
	emit_signal("mission_finish")
	player.character.set_paper_visible(false)
	#gui.hide_panel()

signal mission_fail
func fail_mission():
	mission.target.stop()
	has_mission = false
	remove_crumbs(crash_penalty)
	emit_signal("mission_fail")
	player.character.set_paper_visible(false)
	#gui.fail()

func win():
	print('won!')
	var win_menu = preload("res://scenes/WinMenu.tscn").instance()
	gui.add_child(win_menu)
	

func lose():
	print('how the hell did you manage that its literally impossible')


func _process(delta):
	if has_mission:
		mission.update(delta)
		if mission.timed_out():
			fail_mission()
			$FailSound.play()
	if loading_scene:
		var t = OS.get_ticks_msec()
		while OS.get_ticks_msec() < t + 0.1: # use "time_max" to control for how long we block this thread
			# poll your loader
			var err = loader.poll()
			
			if err == ERR_FILE_EOF: # Finished loading.
				_start_game()
				loader = null
				break
			elif err == OK:
				var loading = get_node_or_null("/root/Loading")
				if loading != null:
					loading.set_percent( float(loader.get_stage()) / loader.get_stage_count() * 100.0)
			else: # error during loading
				print("An error ocurred while loading the game.")
				loader = null
				break


signal free_active_scene
func start_game():
	_reset_all()
	get_tree().paused = false
	get_tree().root.add_child(loading_screen.instance())
	
	emit_signal("free_active_scene")
	
	loading_scene = true
	loader = ResourceLoader.load_interactive("res://scenes/Main.tscn")

func _start_game():
	loading_scene = false
	var tree = get_tree()
	tree.root.get_node("Loading").free()
	tree.root.add_child(loader.get_resource().instance())
	# change_scene("res://scenes/Main.tscn")
	

func start_menu():
	get_tree().paused = false
	get_node("/root/Game").queue_free()
	get_tree().change_scene("res://scenes/Menu.tscn")
