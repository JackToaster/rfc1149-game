extends Spatial


func _ready():
	if $"/root/GameGlobals".has_completed_tutorial:
		queue_free()
	else:
		$"/root/GameGlobals".player.can_fly = false
		$"/root/GameGlobals".player.fail_on_crash = false


func _on_FirstTarget_finished():
	$"/root/GameGlobals".player.can_fly = true
	$SecondPaper.fade_in()


func _on_SecondTarget_finished():
	$"/root/GameGlobals".player.fail_on_crash = true
	$TutorialZone.queue_free()
	$"/root/GameGlobals".gui.elapsed_time = 0
	$"/root/GameGlobals".has_completed_tutorial = true
