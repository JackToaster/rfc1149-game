extends Spatial

export(float) var walk_speed_multiplier = 1.0
export(float) var walk_timescale = 6.0
export(float) var flap_interpolation = 0.9

onready var anim_tree = $MovementTree
onready var state_machine = anim_tree["parameters/playback"]

var flap_blend_target = 0
var flap_blend = flap_blend_target

# Called when the node enters the scene tree for the first time.
func _ready():
	anim_tree.set("parameters/WalkBlend/TimeScale/scale", walk_timescale)
	GameGlobals.connect("mission_finish", self, "coo")

func walk():
	state_machine.travel("WalkBlend")

func set_walk_speed(speed: float):
	var abs_speed = abs(speed)
	anim_tree.set("parameters/WalkBlend/BlendAmount/blend_amount", clamp(abs_speed * walk_speed_multiplier, 0, 1))

func fly():
	state_machine.travel("FlyBlend")
	flap_blend = 1

func set_flap(flap: bool):
	if flap:
		flap_blend_target = 1
	else:
		flap_blend_target = 0

func _process(delta):
	flap_blend = flap_interpolation * flap_blend + (1.0 - flap_interpolation) * flap_blend_target
	anim_tree.set("parameters/FlyBlend/FlapBlend/blend_amount", flap_blend)
	
	if Input.is_action_pressed("coo") and not $"Coo sound".playing:
		$"Coo sound".play_rand()

func crash():
	state_machine.travel("Crashed")
	$"Crash sound".play_rand()

func coo():
	print('coo coo')
	$"Coo sound".play_rand()

func set_paper_visible(visible: bool):
	$"Armature/Skeleton/BeakAttachment/paper-held".visible = visible


# To make the character not flap when starting flying in the menu
#func set_flying_no_takeoff():
#	state_machine.start("FlyBlend")
