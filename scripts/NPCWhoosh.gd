extends AudioStreamPlayer3D


export(float) var angular_vel_factor = 8
export(float) var volume_fac = 18
export(float) var base_volume = -30
export(float) var zero_speed_volume = 0.001
export(float) var pitch_scale_fac = 0.2

func _ready():
	play()
	unit_db = -80

func _process(delta):
	var speed = get_parent().speed
	pitch_scale = speed * pitch_scale_fac
	var unit_db_ = log(speed + zero_speed_volume) * volume_fac + base_volume
	#print(unit_db_)
	unit_db = unit_db_
