extends ColorRect

enum Mode{
	fade_in,
	fade_out
}

signal fade_completed

export(Mode) var mode
export(bool) var autostart

func _ready():
	if mode == Mode.fade_in:
		visible = true
		color = Color(1,1,1,1)
	else:
		visible = false
	
	if autostart:
		play()

func play():
	var tween = $Tween
	if mode == Mode.fade_in:
		tween.interpolate_property(self, "color", Color(1,1,1,1), Color(1,1,1,0), 1, Tween.TRANS_LINEAR)
	else:
		visible = true
		tween.interpolate_property(self, "color", Color(1,1,1,0), Color(1,1,1,1), 1, Tween.TRANS_LINEAR)
	tween.start()


func _on_Tween_tween_all_completed():
	emit_signal("fade_completed")
	if mode == Mode.fade_in:
		queue_free()
