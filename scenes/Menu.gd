extends Control

export(NodePath) var menu_root

func _ready():
	GameGlobals.connect("free_active_scene", self, "free_active_scene")

func _on_Play_pressed():
	$FadeRect.play()

func _on_FadeRect_fade_completed():
	$"/root/GameGlobals".start_game()

func _on_About_pressed():
	$about.visible = true

func _on_close_pressed():
	$about.visible = false

func free_active_scene():
	get_node(menu_root).queue_free()
