extends ColorRect

export(NodePath) var gui_path
onready var gui = get_node(gui_path)

var tree_paused
var gui_paused

enum State{
	paused,
	unpaused
}
var state = State.unpaused

var accepting_inputs = true

func _ready():
	visible = false
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)

func _process(delta):
	if accepting_inputs:
		if Input.is_action_just_pressed("ui_cancel"):
			_toggle_pause()

func _toggle_pause():
	if state == State.unpaused:
		pause()
	else:
		unpause()

func pause():
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	tree_paused = get_tree().paused
	get_tree().paused = true
	gui_paused = gui.paused
	gui.paused = true
	gui.anim_player.pause_mode = PAUSE_MODE_STOP
	state = State.paused
	$AnimationPlayer.play("Appear")

func unpause():
	$AnimationPlayer.play("Dissapear")

func _unpause():
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	get_tree().paused = tree_paused
	gui.paused = gui_paused
	gui.anim_player.pause_mode = PAUSE_MODE_PROCESS
	state = State.unpaused

func _on_AnimationPlayer_animation_finished(anim_name):
	match anim_name:
		"Appear":
			pass
		"Dissapear":
			_unpause()


func _on_Resume_pressed():
	if state == State.paused:
		unpause()


enum FadeCompleteAction{
	restart,
	exit
}
var fade_complete_action

func _on_Restart_pressed():
	fade_complete_action = FadeCompleteAction.restart
	accepting_inputs = false
	$FadeRect.play()


func _on_Exit_pressed():
	fade_complete_action = FadeCompleteAction.exit
	accepting_inputs = false
	$FadeRect.play()

func _on_FadeRect_fade_completed():
	match fade_complete_action:
		FadeCompleteAction.restart:
			$"/root/GameGlobals".start_game()
		FadeCompleteAction.exit:
			$"/root/GameGlobals".start_menu()
