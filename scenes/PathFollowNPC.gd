extends PathFollow

enum Movement{
	walking,
	gliding,
	flapping
}

export(Movement) var movement_type
export(float) var speed = 1
export(bool) var paper = true
export(float) var flying_bank_multiplier = 1.5
export(float) var bank_smoothing = 0.9

var last_rotation = 0
var last_position
var ang_vel = 0

func _ready():
	last_position = global_transform.origin
	if movement_type == Movement.walking:
		$character.walk()
		$character.set_walk_speed(1)
	elif movement_type == Movement.gliding:
		$character.fly()
		$character.set_flap(0)
	elif movement_type == Movement.flapping:
		$character.fly()
		$character.set_flap(1)
		
	$character.set_paper_visible(paper)

func _process(delta):
	offset += delta * speed
	var translation = global_transform.origin - last_position
	last_position = global_transform.origin
	
	translation.y = 0
	global_transform = global_transform.looking_at(global_transform.origin + translation, Vector3.UP)
	
	ang_vel = (rotation.y - last_rotation) / (delta + 0.01)
	last_rotation = rotation.y
	$character.rotation.z = lerp(ang_vel * flying_bank_multiplier, $character.rotation.z, bank_smoothing)
