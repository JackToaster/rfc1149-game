tool
extends ImmediateGeometry

export(int) var num_points = 50
export(float) var point_scale = 1.0
export(Vector3) var up_vec = Vector3.UP
export(Curve) var weight_curve
export(float) var fade_in_time = 1.0
export(float) var fade_out_time = 0.5


const left_offset = Vector3(-1, 0, 0)
const right_offset = Vector3(1, 0, 0)

var points = []

var points_count = 0

var previous_position = Vector3(0,0,0)

var enabled: bool = false
var fade_pct = 0

func _ready():
	points = []
	points_count = 0
	fade_pct = 0

func _process(_delta):
	points_count += 1
	
	if points_count > num_points:
		points.pop_front()
		points_count -= 1
	
	var new_position = global_transform.origin
	
	var faded_point_scale = fade_pct * point_scale
	if new_position != previous_position and faded_point_scale > 0:
		var direction = global_transform.looking_at(previous_position, up_vec)
		direction.basis = direction.basis.scaled(Vector3(faded_point_scale, faded_point_scale, faded_point_scale))
		points.push_back(direction)
		previous_position = new_position
	else:
		points.push_back(null)
	_draw()

func _draw():
	# Clean up before drawing.
	clear()

	# Begin draw.
	begin(Mesh.PRIMITIVE_TRIANGLE_STRIP)
	var global_rotation = global_transform.basis
	
	for i in range(len(points)):
		var point = points[i]
		if point != null:
			set_normal(global_rotation.xform_inv(point.basis.xform(up_vec)))
			
			var uv_y = i / float(len(points))
			var curve_weight = weight_curve.interpolate_baked(1.0 - uv_y)
			
			set_uv(Vector2(0, uv_y))
			add_vertex(global_transform.xform_inv(point.xform(left_offset * curve_weight)))
			
			set_uv(Vector2(1, uv_y))
			add_vertex(global_transform.xform_inv(point.xform(right_offset * curve_weight)))
		

	# End drawing.
	end()

func set_point_scale(scale_pct: float):
	fade_pct = scale_pct

func fade_in():
	if not enabled:
		$FadeTween.stop_all()
		$FadeTween.interpolate_method(self, "set_point_scale", fade_pct, 1.0, fade_in_time, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
		$FadeTween.start()
	enabled = true

func fade_out():
	if enabled:
		$FadeTween.stop_all()
		$FadeTween.interpolate_method(self, "set_point_scale", fade_pct, 0.0, fade_out_time, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
		$FadeTween.start()
	enabled = false
	
