# Wi-Fly

A game about RFC-1149, A Standard for the Transmission of IP Datagrams on Avian Carriers.

Earn breadcrumbs by delivering packets from point to point. Be careful not to crash or  let the packet time out or it will be lost!
Earn 100 breadcrumbs to win. You can also keep playing and see how many breadcrumbs you can get.


Controls:
Arrow keys/WASD: Move
Shift: Fly
Press the Up arrow or W while flying to speed up
C: Coo


Created with the Godot game engine for the 21st Godot Wild Game Jam in May 2020.

Game & artwork by Jack LeFevre
Music by Adam LeFevre

Credit for memes included in this game goes to the following Reddit users:
u/123ikl
u/SpinnyBoye
u/geziai
u/pigeonloverc00
u/herr_johannes
u/Kav147
u/Alertrobotdude
u/chaosgiantmemes